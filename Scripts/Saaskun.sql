
create database if not exists saaskun;
use saaskun;

create table if not exists usuario (
	id_usuario 	int(11)		auto_increment,
    nombre		varchar(40) not null,
	username		varchar(15) not null,
    apellidos	varchar(50)	not null,
    telefono	varchar(10) null,
    grupo 		varchar(20) null,
	password	varchar(60) 	not null,
	email	varchar(40)		not null,
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    primary key(id_usuario) )
    Engine= InnoDB 
    character set latin1 
    auto_increment = 1; 

create table if not exists nodo (
	id_nodo 	int(3)		auto_increment,
    tipo_nodo	varchar(15)	not null,
	ubicacion  varchar (20) not null,
    -- consumo	varchar(10) not null,
	-- temperatura	varchar(10)  null,
	-- tiempo	varchar(30)		not null,
    nombrenodo varchar(20) not null,
    -- status_nodo varchar(5) not null,
    ip varchar (20) not null,
    -- lumens varchar (10) null,
    fk_id_usuario int(11)  null,
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    primary key(id_nodo))
    Engine= InnoDB 
    character set latin1 
    auto_increment = 1;
    
    
 CREATE TABLE IF NOT EXISTS `sessions` (
  session_id varchar(128) COLLATE utf8mb4_bin NOT NULL,
  expires int(11) unsigned NOT NULL,
  data text COLLATE utf8mb4_bin,
  PRIMARY KEY (session_id)
) ENGINE=InnoDB;

CREATE TABLE bitacora (
	id_bitacora int(3)		auto_increment,
    nombreusuario	varchar(10) not null,
    camposanteriores varchar(100) not null,
    camposnuevos varchar (100) not null,
    fechacambio date,
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    primary key(id_bitacora))
    Engine= InnoDB 
    character set latin1 
    auto_increment = 1;

CREATE TABLE nodoinfo(
    id_nodo INT(10) NOT NULL,
    consumo INT(10) NOT NULL,
	temperatura float(5,2) NOT NULL,
    lumens INT(5) NOT NULL,
    estado INT(1) NOT NULL,
    chip_id varchar(10) NOT NULL,
    tiempo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
    Engine= InnoDB 
    character set latin1 
    auto_increment = 1;



ALTER TABLE nodoinfo ADD FOREIGN KEY (id_nodo) REFERENCES nodo (id_nodo);
Alter table nodo ADD FOREIGN KEY (fk_id_usuario) REFERENCES usuario (id_usuario);
/*
insert into Usuario (nombre, username, apellidos, telefono,grupo, password, email)
			values  ("Ivonne","Ivonnsita","Martinez Ruiz", "6145875234", "Casita", "123", "ivonne@gmail.com"),
					("Kevin","ElKevin","Rivas Aguilar", "6142345678", "escuela", "456","kevin@gmail.com"),
                    ("Luis","Memo","Rivera Yanez", "6141589674", "Oficina","789", "memo@gmail.com");

Select *
From usuario;
                    
                    
insert into Nodo    (ubicacion, tipo_nodo, 
                    -- consumo,
                    -- temperatura, 
                    -- tiempo, 
                    ip, 
                    status_nodo, 
                    -- lumens,
                    fk_id_usuario) 
			values  ("Lampara1",    "Luz",      "192.180.52.1",     1,1),
					("Cuarto",      "Aire",     "192.180.25.4",     0,2),
                    ("Cuarto",      "Luz",      "192.180.25.1",     1,1),
         
Select *
From Nodo;
*/
-- Registrar un usuario
CREATE PROCEDURE registrousuario (  IN nom1 VARCHAR(40), IN usrnme VARCHAR(15), 
                                    IN ape1 VARCHAR(50), IN tel VARCHAR(10),
                                    IN gpo VARCHAR(20), IN psswd VARCHAR(60), IN correo VARCHAR(40))
INSERT  INTO usuario(id_usuario, nombre, username, apellidos, telefono, grupo, password, email)
        VALUES (0,nom1, usrnme, ape1, tel, gpo, psswd, correo);

-- Registrar un nodo
CREATE PROCEDURE registronodo ( IN t_nd VARCHAR(15), IN ubic VARCHAR(20), IN nombnod varchar(20),
                                IN dirip VARCHAR(15)  ,
                                IN fk_usu INT(11))
INSERT  INTO nodo (tipo_nodo,ubicacion,nombrenodo,ip, fk_id_usuario)
        VALUES (t_nd,ubic,nombnod,dirip, fk_usu );




-- Acutalizar usuario
CREATE PROCEDURE actuau (IN AU INT(11), IN nom1 VARCHAR(40), IN usrnme VARCHAR(15), 
                                    IN ape1 VARCHAR(50), IN tel VARCHAR(10),
                                    IN gpo VARCHAR(20), IN psswd VARCHAR(60), IN correo VARCHAR(40))
UPDATE usuario SET nombre=nom1, username=usrnme, apellidos=ape1, telefono=tel, grupo=gpo, password=psswd, email=correo
WHERE id_usuario=AU;

-- Actualizar Nodo
CREATE PROCEDURE actuan (  IN AN INT(11),  IN t_nd VARCHAR(15), IN ubic VARCHAR(20), 
                            IN nnod varchar(20),IN dirip VARCHAR(15) , IN fk_usu INT(3))
UPDATE nodo SET     tipo_nodo=t_nd,ubicacion=ubic,nombrenodo=nnod,ip=dirip ,fk_id_usuario=fk_usu  WHERE id_nodo = AN;




-- Borrar usuarios
CREATE PROCEDURE borraru (IN BU INT(11))
DELETE FROM usuario WHERE id_usuario=BU;

-- Borrar nodo
CREATE PROCEDURE borrarn (IN BN INT(3))
DELETE FROM nodo WHERE id_nodo=BN;





-- Insertar informacion al Nodo
CREATE PROCEDURE infonodo ( IN idnodo INT(10), IN consunodo INT(10),
                            IN tempnodo float(5,2), IN lumnodo INT(5),
                            IN estnodo INT(1), IN chipid varchar(10))
INSERT  INTO nodoinfo (id_nodo,consumo,temperatura,lumens, estado, chip_id )
        VALUES (idnodo,consunodo,tempnodo,lumnodo,estnodo, chipid);





-- SELECT para los usuarios
CREATE PROCEDURE selecunom (IN L VARCHAR(30))
SELECT * FROM usuario WHERE	nombre       LIKE concat(L)
OR                          			username     LIKE concat(L)
OR                         			 	apellidos    LIKE concat(L);
-- DROP PROCEDURE selecunom;

-- SELECT para usuarios con id
CREATE PROCEDURE selecu (IN L int(11))
SELECT * FROM usuario WHERE	id_usuario       LIKE concat(L);
-- DROP PROCEDURE selecu;

-- Ver Nodo
CREATE PROCEDURE verno (IN VN INT(11))
SELECT * FROM nodo WHERE id_nodo=VN;

/*
-- Llamadas para registrar
CALL registrousuario('Jose','JoseAntonio53','Mercedes','6143944859','Casa1','josemercedes1945','joseantonio@gmail.com');
CALL registronodo('Sala','Comedor','Luz','250','25','15:36','172.16.155.3',1,NULL,1);  ESTE YA NO FUNCIONA

-- Llamadas para actualizar
CALL actuau(3,'Marcos','Marquitos','Garza','6147891235','Casa1','marcosvolador07','marcosgarza@gmail.com');
CALL actuan(1,'Sala','Sala','Televisor','350','26','17:41','172.16.155.5',1,NULL,1);

-- Llamada insertar en nodo
-- CALL infonodo('Marcos','');

-- Llamadas para visualizar
CALL selecunom('Jose');
CALL selecu(1);
CALL verno(2);

-- Llamadas para borrar
CALL borraru(4);
CALL borrarn(2);


*/




