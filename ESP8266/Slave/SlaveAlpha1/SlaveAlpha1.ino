#include "connections.h"
#include "hw_timer.h"
#include "dimmer.h"
#include "acs712.h"
void setup() 
{
  Serial.begin(115200);
  
  pinMode(ZERO_CROSSING_INT_PIN,INPUT); //pin 12 , input
  
  for(int i=0; i<NUM_CHANNELS; i++) // NUM_CHANNELS = 2
  {
    pinMode(Drive_Pin[i],OUTPUT); // Drive_pin = 5,4
    digitalWrite(Drive_Pin[i],LOW);
  }
   
  connectToWiFi(); //Funcion para conectar al wifi 
  WebSocketConnect(); //funcion para iniciar websocket
  MDNSConnect(); //iniciar MDNS
  HTTPUpdateConnect();

  
  noInterrupts();
  
  timer_init();
  attachInterrupt(digitalPinToInterrupt(ZERO_CROSSING_INT_PIN),Zero_Crossing_Int,RISING); 
  
  interrupts();
}


void loop() 
{

  if(millis() - lastConnectivityCheckTime > 1000)
  {
    if(WiFi.status() != WL_CONNECTED) 
    {
      connectToWiFi();
      WebSocketConnect();
      MDNSConnect();
      
    }  
    lastConnectivityCheckTime = millis();
  }
  
  else 
  {
    webSocket.loop();
    yield();
    
    //OTA
    if (millis() - lastTimeHost > 10) 
    {
      httpServer.handleClient();
      lastTimeHost = millis();
    }

    //Update Connected Clients
    currentChangeTime = millis();
    if(currentChangeTime - lastChangeTime> 300 && isSynced == 0) 
    {
      String websocketStatusMessage = "A" + String(Dimming_Lvl[0]) + ",B" + String(Dimming_Lvl[1]) + ",X" + String(State[0]) + ",Y" + String(State[1]);
      webSocket.broadcastTXT(websocketStatusMessage); // Tell all connected clients current state of the channels
      isSynced = 1;
    }
  }
  //funcion para leer el modulo acs712
  //leer_amp();
}
