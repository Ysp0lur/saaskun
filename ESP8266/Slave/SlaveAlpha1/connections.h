
#include <FS.h>                   //Este tiene que estar primero, para evitar que todo falle...
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <SPI.h>
#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

//define sus Valores default, si son diferentes a los valores en config.json, se sobreescriben.
//longitud maxima de + 1 
char mqtt_server[40];
char mqtt_port[6] = "8080";
char blynk_token[33] = "SU_TOKEN_REDCORP";
//default custom static IP
char static_ip[16] = "192.168.0.200";
char static_gw[16] = "192.168.0.1";
char static_sn[16] = "255.255.255.0";

//bandera para guardar los datos
bool shouldSaveConfig = false;


const char *APssid = "RedCorp_Dimmer";
const char *APpassword = "1234";
const char *host = "REDCorpAP";
WiFiServer server = WiFiServer(80);

///////////////////////////////////////////////////////////////////DEVICE CONFIG////////////////////////////////////////////////////////////////////

void saveConfigCallback () {
  Serial.println("Debe Guardar Configuracion");
  shouldSaveConfig = true;
}

void connectToWiFi() 
{
  Serial.println();

  //FS Limpias, para las Pruebas
  //SPIFFS.format();

  //Lee Configuracion de FS json
  Serial.println("Montando FS...");

  if (SPIFFS.begin()) {
    Serial.println("Montando Sistema de archivos");
    if (SPIFFS.exists("/config.json")) {
      //El Archivo Existe, Leyendo y Cargando
      Serial.println("Leyendo Archivo de Configuracion");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("Abriendo Archivo de Configuracion");
        size_t size = configFile.size();
        // Asignar búfer para almacenar contenido del archivo.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nAnalizando json");

          strcpy(mqtt_server, json["mqtt_server"]);
          strcpy(mqtt_port, json["mqtt_port"]);
          strcpy(blynk_token, json["blynk_token"]);

          if(json["ip"]) {
            Serial.println("Configurar IP Personalizado Desde config");
            //static_ip = json["ip"];
            strcpy(static_ip, json["ip"]);
            strcpy(static_gw, json["gateway"]);
            strcpy(static_sn, json["subnet"]);
            //strcat(static_ip, json["ip"]);
            //static_gw = json["gateway"];
            //static_sn = json["subnet"];
            Serial.println(static_ip);
/*            Serial.println("converting ip");
            IPAddress ip = ipFromCharArray(static_ip);
            Serial.println(ip);*/
          } else {
            Serial.println("IP no Personalizada en config");
          }
        } else {
          Serial.println("Fallo al Cargar Configuracion de json");
        }
      }
    }
  } else {
    Serial.println("Fallo al Montar Sistema de Archivos");
  }
  //Fin de Lectura
  Serial.println(static_ip);
  Serial.println(blynk_token);
  Serial.println(mqtt_server);


  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 5);
  WiFiManagerParameter custom_blynk_token("blynk", "blynk token", blynk_token, 34);


  //WiFiManager
  //inicialización local. Una vez que se lleva a cabo su actividad, no hay necesidad de mantenerlo en torno
  WiFiManager wifiManager;

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  //set static ip
  IPAddress _ip,_gw,_sn;
  _ip.fromString(static_ip);
  _gw.fromString(static_gw);
  _sn.fromString(static_sn);

  wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);
  
  //añadir todos sus parámetros aquí
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_blynk_token);

  //Reiniciar opciones - para testeo
  //wifiManager.resetSettings();

  //establecer la calidad mínima de la señal por lo que no tiene en cuenta los AP en virtud de calidad
  //menor a 8%
  wifiManager.setMinimumSignalQuality();
  
  //Establece el tiempo de espera hasta que el portal de configuración se apaga
  //Util para volver a intentar todo o mandar a dormir
  //en segundos
  //wifiManager.setTimeout(120);

  //Busca ssid y pass y trata de conectarse
  //Si no conecta, inicia un punto de acceso con el nombre especificado
  //"REDCorpAP"
  //entra en un bucle de bloqueo en espera de la configuración
  if (!wifiManager.autoConnect(host, "password")) {
    Serial.println("Fallo de Coneccion y Tiempo de Espera");
    delay(3000);
    //reiniciar y volver a intentarlo, o tal vez lo puso al sueño profundo
    ESP.reset();
    delay(5000);
  }

  //Si has llegado hasta aquí se ha conectado a la red WiFi
  Serial.println("Conexion Lista");

  //Leer parámetros actualizados
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(blynk_token, custom_blynk_token.getValue());

  //Guardar los parámetros personalizados en FS
  if (shouldSaveConfig) {
    Serial.println("Guardando Configuracion");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["blynk_token"] = blynk_token;

    json["ip"] = WiFi.localIP().toString();
    json["gateway"] = WiFi.gatewayIP().toString();
    json["subnet"] = WiFi.subnetMask().toString();

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("Fallo al abrir el archivo de configuración para la escritura");
    }

    json.prettyPrintTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //guarda
  }

  Serial.println("IP Local");
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.gatewayIP());
  Serial.println(WiFi.subnetMask());
 
}
