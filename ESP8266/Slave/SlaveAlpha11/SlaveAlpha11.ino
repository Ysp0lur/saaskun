#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "hw_timer.h"
#include "html_pages.h"

// WIFI SETTINGS
const char* ssid = "HITRON-BFF0";
const char* password = "87ZPJWLP8UO6";

// PIN SETTINGS
const byte switchPin = 4;
const byte zcPin = 12;
const byte outPin = 5;

byte fade = 1;
byte state = 0;
byte tarBrightness = 0;
byte curBrightness = 0;
byte zcState = 0; // 0 = ready; 1 = processing;

ESP8266WebServer server(80);
WiFiClient espClient;

void setup(void) {
  pinMode(zcPin, INPUT_PULLUP);
  pinMode(switchPin, INPUT_PULLUP);
  pinMode(outPin, OUTPUT);
  
  digitalWrite(outPin, 0);
  
  Serial.begin(115200);
  Serial.println("");

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.print("\nConnected: ");
  Serial.println(WiFi.localIP());

  server.on("/", []() {
    // server args for state
    if (server.arg("s") != "") {
      if (server.arg("s") == "1" || server.arg("s") == "on" || server.arg("s") == "true") {
        updateState(1);
      }
      else if (server.arg("s") == "t") {
        updateState(!state);
      }
      else {
        updateState(0);
      }
    }
    
    // server args for brightness
    if (server.arg("b") != "") {
      updateBrightness((byte) server.arg("b").toInt());
    }
    
    // server args for fade
    if (server.arg("f") != "") {
      if (server.arg("f") == "1" || server.arg("f") == "on" || server.arg("f") == "true") {
        updateFade(1);
      }
      else if (server.arg("f") == "t") {
        updateFade(!fade);
      }
      else {
        updateFade(0);
      }
    }
    
    // json output
    String s = "{\n   \"s\":";
    s += state;
    s += ",\n   \"b\":";
    s += tarBrightness;
    s += ",\n   \"f\":";
    s += fade;
    s += "\n}";
    
    server.send(200, "text/plain", s);
  });

  server.on("/webupdate", []() {
    server.send(200, "text/html", updateHTTP);
  });
  
  server.onFileUpload([]() {
    if(server.uri() != "/update") return;
    detachInterrupt(zcPin);
    detachInterrupt(switchPin);
    hw_timer_set_func(0);
    digitalWrite(outPin, 0);
    HTTPUpload& upload = server.upload();
    if(upload.status == UPLOAD_FILE_START){
      state = 0;
      //Serial.setDebugOutput(true);
      Serial.printf("Update: %s\n", upload.filename.c_str());
      uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
      if(!Update.begin(maxSketchSpace)){//start with max available size
        Update.printError(Serial);
      }
    } else if(upload.status == UPLOAD_FILE_WRITE){
      if(Update.write(upload.buf, upload.currentSize) != upload.currentSize){
        Update.printError(Serial);
      }
    } else if(upload.status == UPLOAD_FILE_END){
      if(Update.end(true)){ //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
      //Serial.setDebugOutput(false);
    }
    yield();
  });
  
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError())?"UPDATE FAIL":"UPDATE SUCCESS");
    ESP.restart();
  });
  
  server.begin();
  Serial.println("HTTP server: Started");
  


  hw_timer_init(NMI_SOURCE, 0);
  hw_timer_set_func(dimTimerISR);
  
 
  attachInterrupt(zcPin, zcDetectISR, RISING);
}
  

void loop(void)
{
 
  // handle http:
  server.handleClient();
 
}

void updateState(bool newState) {
  state = newState;
}

void updateFade(bool newFade) {
  fade = newFade; 
}

void updateBrightness(int newBrightness) {
  tarBrightness = newBrightness; 
}


void dimTimerISR() {
    if (fade == 1) {
      if (curBrightness > tarBrightness || (state == 0 && curBrightness > 0)) {
        --curBrightness;
      }
      else if (curBrightness < tarBrightness && state == 1 && curBrightness < 255) {
        ++curBrightness;
      }
    }
    else {
      if (state == 1) {
        curBrightness = tarBrightness;
      }
      else {
        curBrightness = 0;
      }
    }
    
    if (curBrightness == 0) {
      state = 0;
      digitalWrite(outPin, 0);
    }
    else if (curBrightness == 255) {
      state = 1;
      digitalWrite(outPin, 1);
    }
    else {
      digitalWrite(outPin, 1);
    }
    
    zcState = 0;
}

void zcDetectISR() {
  if (zcState == 0) {
    zcState = 1;
  
    if (curBrightness < 255 && curBrightness > 0) {
      digitalWrite(outPin, 0);
      
      int dimDelay = 30 * (255 - curBrightness) + 400;
      hw_timer_arm(dimDelay);
    }
  }
}
