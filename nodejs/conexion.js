let mysql = require("mysql");
const OPCIONES ={ 
    host: "localhost",
    user: "root",
    password: "",
    database: "base",
    insecureAuth : true,
    port: 3306
}
let conexion=mysql.createConnection(OPCIONES);

conexion.connect((error)=>{
if(error){
    console.log("error al conectar: " + error.code);
}
else console.log("conexion establecida con la base: " + OPCIONES.database);

});
