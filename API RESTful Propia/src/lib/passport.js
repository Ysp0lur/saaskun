const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const pool = require('../database');
const helpers = require('./helpers');

passport.use('local.signin', new LocalStrategy({
usernameField: 'username',
passwordField: 'password',
passReqToCallback: true
}, async (req, username, password, done) =>{
 //   console.log(req.body);
   const rows = await pool.query('SELECT * FROM usuario WHERE username = ? ',[username]);
 //  console.log(rows);
    if (rows.length > 0){
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.password);
        if (validPassword) {
            done(null, user, req.flash('success','Bienvenido  ' + user.username));
        } else{
            done(null, false, req.flash('message','Password Incorrecta'));
        }
    } else {
        return done(null, false, req.flash('message','Nombre de Usuario no Existe'));
    }
}));






/*----------------------------------------------------------------------------------------------- */

passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) =>{
    const { nombre, apellidos, telefono, grupo, email } = req.body;
        const newUser ={
            nombre,
            username,
            apellidos,
            telefono,
            grupo,
            password,
            email
// ' CALL registrousuario("'+nombre+'","'+username+'","'+apellidos+'","'+telefono+'","'+grupo+'","'+newUser.password+'","'+email+'") '
        };
        newUser.password= await helpers.encryptPassword(password);
        const result = await pool.query('INSERT INTO usuario SET ?',[newUser]);
        newUser.id_usuario = result.insertId;
        return done(null, newUser);
}));

passport.serializeUser((user, done) => {
    done(null, user.id_usuario);
});

passport.deserializeUser( async (id_usuario, done) =>{
  const rows = await pool.query('SELECT * FROM usuario WHERE id_usuario = ?',[id_usuario]);
   done(null, rows[0]);
});
