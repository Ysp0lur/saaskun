import React, { Component } from 'react';


class Editar extends Component{

    constructor(){
        super();
        this.state={
            id_usuario:'',
            nombre:'',
            username:'',
            apellidos:'',
            telefono:'',
            grupo:'',
            password:'',
            email:''
        };
        this.handleChange = this.handleChange.bind(this);
        this.adduser = this.adduser.bind(this);
    }
// Funcion Agregar/Editar segun si llega limpio ID o no
    adduser(e){
        if(this.state.id_usuario){
            fetch(`/links/edit/${this.state.id_usuario}`,{
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                M.toast({html: 'Usuario Actualizado'});
                this.setState({
                id_usuario:'',
                nombre:'',
                username:'',
                apellidos:'',
                telefono:'',
                grupo:'',
                password:'',
                email:''});
            });
        } else{

            fetch('/links/add', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data =>{
                console.log(data)
                M.toast({html: 'Usuario Agregado'});
                this.setState({nombre:'',
                username:'',
                apellidos:'',
                telefono:'',
                grupo:'',
                password:'',
                email:''});
            })
            .catch(err => console.error(err));
            e.preventDefault();
        }
    }
    
   editUser(id){
       fetch(`/links/edit/${id}`)
       .then(res => res.json())
       .then(data => {
           console.log(data)
           this.setState({
           nombre: data.nombre,
           username: data.username,
           apellidos: data.apellidos,
           telefono: data.telefono,
           grupo: data.grupo,
           password: data.password,
           email: data.email

           })
       });
   }








    handleChange(e){
       const { name, value } = e.target;
       this.setState({
           [name]: value
       }); 
    }
    









    render(){
        return(
            <div>
            {/*Esto es la Navegacion*/}
            <nav className="light-blue darken-4">
                <div className="container">
                    <a className="brand-logo" href="/">SaasKun Editar</a>
                </div>
            </nav>
            <div className="container">
                <div className="row">
                    <div className="col s5">
                        <div className="card">
                            <div className="card-content">
                                <form onSubmit={this.adduser}>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">account_circle</i>
                                        <label>Nombre</label>
                                        <input name="nombre" onChange={this.handleChange} type="text" placeholder="Nombre" value={this.state.nombre} required></input>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">face</i>
                                        <label>Username</label>
                                        <input name="username" onChange={this.handleChange} type="text" placeholder="Username" value={this.state.username} required></input>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">more</i>
                                        <label>Apellidos</label>
                                            <input name="apellidos" onChange={this.handleChange} type="text" placeholder="Apellidos" value={this.state.apellidos} required></input>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">phone_iphone</i>
                                        <label>Telefono</label>
                                            <input name="telefono" onChange={this.handleChange} type="tel" pattern="[0-9]{10}" placeholder="Telefono a 10 Digitos" value={this.state.telefono} required></input>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">group</i>
                                        <label>Grupo</label>
                                            <input name="grupo" onChange={this.handleChange} type="text" placeholder="Grupo" value={this.state.grupo} required></input>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">enhanced_encryption</i>
                                        <label>Password Minimo 8 Digitos</label>
                                            <input name="password" onChange={this.handleChange} type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" placeholder="número, mayúscula y minúscula" value={this.state.password} required></input>
                                       </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12">
                                        <i className="material-icons prefix">email</i>
                                        <label>Email</label>
                                            <input name="email" onChange={this.handleChange} type="email" placeholder="Email" value={this.state.email} required></input>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn waves-effect waves-light" >Registrar <i className="material-icons right">send</i></button>
                                    <button type="" className="btn waves-effect waves-light red" style={{margin:'4px'}} >Borrar <i className="material-icons right">delete</i></button>
                                </form>
                        </div>

                    </div>

                    </div>

                </div>
            </div>

            </div>
            );
    }
}
export default Editar;