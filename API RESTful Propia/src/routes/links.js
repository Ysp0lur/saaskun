const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const mqttHandler =  require ('../MQTT');

const pool = require('../database');
const { isLoggedIn } = require('../lib/auth');
// Multer
const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/uploadsdni'),
    filename:(req, file, cb) =>{
        cb(null, req.user.username + ".jpg");
    }
});


 const upload = multer({
    storage,
    dest: path.join(__dirname, '../public/uploadsdni'),
    limits: {fileSize: 2000000},
    fileFilter: (req, file, cb) => {
        const filetypes = /jpeg|JPEG|JPG|jpg|PNG|png|GIF|gif/;
        const mimetype = filetypes.test(file.mimetype);
        const extname = filetypes.test(path.extname(file.originalname));
        if (mimetype && extname){
            return cb(null, true);
        }
       return cb(null,false, req.flash('message','Archivo Incorrecto'));
    }
}).single('imagedni');



router.post('/uploaddni', isLoggedIn, (req, res) => {
    upload(req,res,function(err) {
        if(err) {
            req.flash('message', 'Archivo Demasiado Grande');
              return res.redirect("/profile");

        }else{
            
          return res.redirect('/profile');
        }
    });
});
// Termina Multer

// Navegacion Basica
router.get('/privacidad', (req, res) => {
    res.render('links/privacidad');
});


// Crear alarmas
router.get('/alarmas', isLoggedIn, (req, res) => {
    res.render('links/alarmas');
});

//Graficar datos
router.get('/graficas', isLoggedIn, (req, res) => {
    res.render('links/graficas');
});

//Exportar Informacion
router.get('/exportar', isLoggedIn, (req, res) => {
    res.render('links/exportar');
});

router.get('/profileedit', isLoggedIn, (req, res) => {
    res.render('./profileedit.hbs');
});

// Termina Navegacion Basica


// crud de Nodos
// leer nodos
router.get('/addnodo', isLoggedIn, (req, res) => {
    res.render('links/addnodo');
});


// agregar nodo
router.post('/addnodo', isLoggedIn, async (req, res) =>{
    const { id_usuario } = req.user;
    const { tipo_nodo, ubicacion, nombrenodo, ipnodo } = req.body;
    const newLink = {
        tipo_nodo,
        ubicacion,
        nombrenodo,
        ipnodo,
        id_usuario
    };
    await pool.query('CALL registronodo ("'+tipo_nodo+'","'+ubicacion+'","'+nombrenodo+'","'+ipnodo+'", "'+id_usuario+'")');
    console.log(newLink);
    req.flash('success', 'Nodo Registrado Correctamente');
    res.redirect('/links');
});

// Seleccionar todos los nodos
router.get('/', isLoggedIn, async (req, res) => {
const links = await pool.query('SELECT * FROM nodo WHERE fk_id_usuario = ? ',[req.user.id_usuario]);
console.log(links);
res.render('links/list',{ links });
});

// Borrar nodos
router.get('/deletenodo/:id_nodo', isLoggedIn, async (req, res) => {
    const { id_nodo } = req.params;
    console.log(req.params.id_nodo);
    await pool.query('CALL borrarn (?)',[id_nodo]);
    req.flash('success', 'Nodo Eliminado Correctamente');
    res.redirect('/links');
});

// Editar Nodo
router.get('/editnodo/:id_nodo', isLoggedIn, async (req, res) =>{
const { id_nodo } = req.params;
const nodo = await pool.query('SELECT * FROM nodo WHERE id_nodo = ? ',[id_nodo]);
console.log(nodo[0]);
res.render('links/editnodo', {nodo: nodo[0]});
});

// Editar nodo 2da Parte
router.post('/editnodo/:id_nodo', isLoggedIn, async (req, res) => {
    const { id_usuario } = req.user;
    const { id_nodo } = req.params;
    const { tipo_nodo, ubicacion, nombrenodo, ipnodo } = req.body;
    const newlink = {
        tipo_nodo,
        ubicacion,
        nombrenodo,
        ipnodo
    };
    console.log(newlink);
    await pool.query('CALL actuan ("'+id_nodo+'","'+tipo_nodo+'", "'+ubicacion+'", "'+nombrenodo+'", "'+ipnodo+'", "'+id_usuario+'")');
    req.flash('success', 'Nodo Actualizado Correctamente');
    res.redirect('/links');
});

// Termina CRUD Nodos

/*---------------------------------------------------------------------------------------------------------------------------------*/


// Actualizar Nodos


router.post('/update/nodo', async (req, res) => {
    const { id_nodo, consumo, temperatura, lumens, estado, chip_id } = req.body;
    const newlink = {
        id_nodo,
        consumo,
        temperatura,
        lumens,
        estado,
        chip_id

    };
    console.log(newlink);  //'INSERT INTO usuario SET ?',[newlink]
    const respuesta = await pool.query('CALL infonodo ("'+id_nodo+'","'+consumo+'", "'+temperatura+'", "'+lumens+'","'+estado+'","'+chip_id+'")');
    console.log(respuesta)
});


// Leer Temperatura
router.get('/read/:id_nodo', async (req, res) =>{
    const { id_nodo } = req.params;
    console.log(id_nodo);
    const links = await pool.query('SELECT * FROM nodoinfo WHERE id_nodo = ? ',[id_nodo]);
    console.log(links);
    res.json(links);
});







// MQTT

const mqttClient = new mqttHandler();
mqttClient.connect();


router.post("/send-mqtt/:id_nodo", function(req, res) {
    mqttClient.sendMessage(req.body.message);
    req.flash('success', 'Cambio de estado el Nodo');
    res.redirect('/links');
  });





/*--------------------------------------------------------------------------------------------------------------------------------*/

// Seleccionar Todo Usuarios
router.get('/select/all', isLoggedIn, async (req, res) => {
    const links = await pool.query('SELECT * FROM usuario');
    console.log(links);
    res.json(links);
});

// Seleccion de 1 usuario 
// router.get('/select/:id_usuario', isLoggedIn, async (req, res) => {
//     const { id_usuario } = req.params;
//     const links = await pool.query('CALL selecu (?)',[id_usuario]);
//     console.log(links);
//     res.json(links);
//     });



//Agregar Usuario
// router.post('/add', isLoggedIn, async (req, res) => {
//     // Aqui necesitamos los campos del Nodo para enlazar a usuario de momento colocamos para el usuario
//     const {nombre, username, apellidos, telefono, grupo, password, email } = req.body;
//     const newLink = {
//                 nombre,
//                 username,
//                 apellidos,
//                 telefono,
//                 grupo,
//                 password,
//                 email
//                      };
//     console.log(newLink);
//     await pool.query('CALL registrousuario("'+nombre+'","'+username+'","'+apellidos+'","'+telefono+'","'+grupo+'","'+password+'","'+email+'")');
    
//     res.json({status: 'Registrado'});
// });
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/
// Editar Usuario
// router.get('/edit/:id_usuario', isLoggedIn, async (req, res) =>{
// const { id_usuario } = req.params;
// const links = await pool.query('CALL selecu(?)',[id_usuario]);
// console.log(links);
// res.json({status: 'Recibido'});
// });

// Actualizar Usuarios --2do paso del edit
router.post('/edit/:id_usuario', isLoggedIn, async(req, res) => {
const { id_usuario } = req.params;
const { password } = req.user;
const { nombre, username, apellidos, telefono, grupo, email } = req.body;
    const newLink = {
                id_usuario,
                nombre,
                username,
                apellidos,
                telefono,
                grupo,
                password,
                email
                     };
    console.log(newLink);
    await pool.query('CALL actuau ('+id_usuario+',"'+nombre+'","'+username+'","'+apellidos+'","'+telefono+'","'+grupo+'","'+password+'","'+email+'")');
    res.redirect('/profile');
});

/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/


//Borrar 1 usuario
// router.get('/delete/:id_usuario', isLoggedIn, async (req, res) => {
// const { id_usuario } = req.params;
// await pool.query('CALL borraru (?)', [id_usuario]);
// console.log('Borrado')
// res.redirect('/links/select/all');
// });

module.exports = router;