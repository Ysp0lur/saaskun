<?php
	
	
	require '../conexion.php';
	session_start();
	
	if(!isset($_SESSION["id_usuario"]) || $_SESSION['tipo_usuario']==1){
		header("Location: ../login.php");
	}
	$cursos = $mysqli->query("SELECT * FROM cursos as c");


?>
<html>
	<head>
		<title>Mis Cursos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../../assets/css/main.css" />
	</head>
	<body class="is-preload">

		<!-- Nav -->
		<nav id="nav">
				<ul class="container">
					<li><a href="../welcome.php">Perfil</a></li>
					<li><a href="../../about.html">Ayuda</a></li>
					<li><a href="../../contact.html">Contactanos</a></li>
					
				</ul>
			</nav>
		

		<!-- Home -->
			<article id="top" class="wrapper style1">
				<div class="container">
					<div class="row">
						<div class="col-4 col-5-large col-12-medium">
							<span class=""><img src="../images/microraptorlogoppal.png" alt="" /></span>
						</div>
						<div class="col-8 col-7-large col-12-medium">
							<header>
								<h1><strong></strong>
                                <h3> Seleccione el curso al que quiere ingresar</h3>
                                </h1>
																</select> </td>
    <td> <select id="id_curso" name="nombre">
				        	<option value="0">Seleccione Curso</option> 
				        	<?php while($lc = $cursos->fetch_assoc()){ ?>
                    
					    	<option value="<?php echo $lc['id_curso']; ?>"><?php echo $lc['nombre']; ?></option>
              
                  <?php }?>
				            </select><td></td><a href="#work" class="button small scrolly">Ir</a>
                    <div>
				            
			                </div></td></td>
																
							</header>
							
						
						</div>
					</div>
				</div>
			</article>

		<!-- Work -->
			<article id="work" class="wrapper style2">
            <h3> La primera característica, orientado a objetos (“OO”), se refiere a un método de programación y al diseño del lenguaje. Aunque hay muchas interpretaciones para OO, una primera idea es diseñar el software de forma que los distintos tipos de datos que usen estén unidos a sus operaciones. Así, los datos y el código (funciones o métodos) se combinan en entidades llamadas objetos. Un objeto puede verse como un paquete que contiene el “comportamiento” (el código) y el “estado” (datos).</h3>
            </article>

		<!-- Portfolio -->
			<article id="portfolio" class="wrapper style3">
            <h3>Video</h3>
            
          <video width="520" height="340" controls>
  <source src="video/java.mp4" type="video/mp4">
            </article>
      
		<!-- Contact -->
			<article id="contact" class="wrapper style4">
				<div class="container medium">
					<header>
						<h2>Comentarios</h2>
					 
					</header>
					<div class="row">
						<div class="col-12">
							<form method="post" action="#">
								<div class="row">
									<div class="col-6 col-12-small">
									
									</div>
									<div class="col-6 col-12-small">
										<input type="text" name="email" id="email" placeholder="Nombre" />
									</div>
									<div class="col-12"></div>
									<div class="col-12">
									  <textarea name="message" id="message" placeholder="Tu Comentario"></textarea>
								  </div>
									<div class="col-12">
										<ul class="actions">
											<li><input type="submit" value="Enviar" /></li>
											<li></li>
										</ul>
									</div>
								</div>
							</form>
						</div>
						<div class="col-12"><!--
								<li><a href="#" class="icon fa-rss"><span>RSS</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span>Instagram</span></a></li>
								<li><a href="#" class="icon fa-foursquare"><span>Foursquare</span></a></li>
								<li><a href="#" class="icon fa-skype"><span>Skype</span></a></li>
								<li><a href="#" class="icon fa-soundcloud"><span>Soundcloud</span></a></li>
								<li><a href="#" class="icon fa-youtube"><span>YouTube</span></a></li>
								<li><a href="#" class="icon fa-blogger"><span>Blogger</span></a></li>
								<li><a href="#" class="icon fa-flickr"><span>Flickr</span></a></li>
								<li><a href="#" class="icon fa-vimeo"><span>Vimeo</span></a></li>
								-->
						  <hr />
						  </ul>
							<hr />
					  </div>
					</div>
					<footer>
						<ul id="copyright">
							
						</ul>
					</footer>
				</div>
			</article>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>