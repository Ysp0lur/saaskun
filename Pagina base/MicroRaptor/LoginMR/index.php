<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Loading MySQL Records on Drop Down Selection using PHP jQuery</title>
<link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" />
<style type="text/css">
select{
 
 width:250px;
 padding:5px;
 border-radius:3px;
}
</style>
<script src="../js/jquery-1.11.2.min.js"></script>
<script><script type="text/javascript">
$(document).ready(function()
{  
 // function to get all records from table
 function getAll(){
  
  $.ajax
  ({
   url: 'Getproducts.php',
   data: 'action=showAll',
   cache: false,
   success: function(r)
   {
    $("#display").html(r);
   }
  });   
 }
 
 getAll();
 // function to get all records from table
 
 
 // code to get all records from table via select box
 $("#getProducts").change(function()
 {    
  var id = $(this).find(":selected").val();

  var dataString = 'action='+ id;
    
  $.ajax
  ({
   url: 'getproducts.php',
   data: dataString,
   cache: false,
   success: function(r)
   {
    $("#display").html(r);
   } 
  });
 })
 // code to get all records from table via select box
});
</script></script>
</head>
<body>

<div class="container">

     <div class="page-header">
        <h3>
        <select id="getProducts">
        <option value="showAll" selected="selected">Show All Products</option>
        <?php
        require_once 'conexion.php';
        
        $stmt = $mysqli->prepare('SELECT * FROM cursos');
        $stmt->execute();
        
        while($row=$stmt->fetch(mysqli::FETCH_ASSOC))
        {
            extract($row);
            ?>
            <option value="<?php echo $cat_id; ?>"><?php echo $cat_name; ?></option>
            <?php
        }
        ?>
        </select>
        </h3>
        </div>
    <blockquote>Load MySQL Records On Drop Down Selection</blockquote>
    <hr />
    
    <div class="" id="display">
       <!-- Records will be displayed here -->
    </div>
    


</div>
</body>
</html>