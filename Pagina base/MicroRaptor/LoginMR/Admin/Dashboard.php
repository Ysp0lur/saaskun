<?php
	
	
	require '../conexion.php';
	session_start();
	
	if(!isset($_SESSION["id_usuario"]) || $_SESSION['tipo_usuario']==2){
		header("Location: ../login.php");
	}
?>
<html>
	<head>
		<title>Crear Cursos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../../assets/css/main.css" />
	</head>
	<body class="is-preload">

		<!-- Nav -->
			<nav id="nav">
				<ul class="container">
					<li><a href="../welcome.php">Perfil</a></li>
					<li><a href="../../about.html">Ayuda</a></li>
					<li><a href="../../contact.html">Contactanos</a></li>
					
				</ul>
			</nav>
		
		<!-- Home -->
			<article id="top" class="wrapper style1">
				<div class="container">
					<div class="row">
						<div class="col-4 col-5-large col-12-medium">
							<span class="image fit"><img src="../../images/pic00.jpg" alt="" /></span>
						</div>
						<div class="col-8 col-7-large col-12-medium">
							<header>
								<h1>Bienvenido <strong>Profesor	</strong>.</h1>
							</header>
							<p>Aqui puede gestionar los cursos que sube a sus alumnos, puede también eliminarlos si lo desea. </a>.</p>
							
                            <a href="#contact" class="button large scrolly">Subir cursos</a>
						</div>
					</div>
				</div>
			</article>

		<!-- Work -->
			<article id="work" class="wrapper style2">
				<div class="container">
					<header>
						<h2>&nbsp;</h2>
					</header>
					<div class="row aln-center">					</div>
					<footer>
						<p>Informacion: </p>
						<p>Alumnos actuales: 1 </p>
					    <p>Cursos subidos: 0 </p>
						<p> Cuatrimestre: 1 </p>
						
						</p>
						<a href="#top" class="button large scrolly">Inicio del perfil</a>
					</footer>
				</div>
			</article>

		<!-- Portfolio -->
			<article id="portfolio" class="wrapper style3">
				<div class="container">
					

		<!-- Contact -->
			<article id="contact" class="wrapper style4">
				<div class="container medium">
					<header>
						<h2>Cursos hacia sus alumnos</h2>
						<p>Aqui puede gestionar sus cursos </p>
					</header>
					<div class="row">
						<div class="col-12">
							<form method="post" action="#">
								<div class="row">
									<div class="col-6 col-12-small">
										<input type="text" name="name" id="name" placeholder="Nombre" />
									</div>
									<div class="col-6 col-12-small">
										<input type="text" name="email" id="email" placeholder="Correo electronico" />
									</div>
									<div class="col-12">
										<input type="text" name="subject" id="subject" placeholder="Asunto" />
									</div>
									<div class="col-12">
										<textarea name="message" id="message" placeholder="Descripcion del curso , instrucciones a sus alumnos"></textarea>
									</div>
									<div class="col-12">
										<ul class="actions">
											<li></li>
											
										</ul>
									</div>
								</div>
							</form>
								<form action="subir.php" method="post" enctype="multipart/form-data"> 
								<br> 
								<input type="text" name="txt" size="20" maxlength="100"> 
								<br> 
								<br> 
								<b>Subir archivo: </b> 
								
								<input name="file" type="file"> 
								<br> 
								<br> 
								
								<input type="submit" value="Subir"> 
								<input type="reset" value="Borrar" class="alt" />
								</form>

						</div>
						<div class="col-12">
							<hr />
							<h3>Redes Sociales</h3>
							<ul class="social">
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
								<li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-tumblr"><span class="label">Tumblr</span></a></li>
								<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
								<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
								<!--
								<li><a href="#" class="icon fa-rss"><span>RSS</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span>Instagram</span></a></li>
								<li><a href="#" class="icon fa-foursquare"><span>Foursquare</span></a></li>
								<li><a href="#" class="icon fa-skype"><span>Skype</span></a></li>
								<li><a href="#" class="icon fa-soundcloud"><span>Soundcloud</span></a></li>
								<li><a href="#" class="icon fa-youtube"><span>YouTube</span></a></li>
								<li><a href="#" class="icon fa-blogger"><span>Blogger</span></a></li>
								<li><a href="#" class="icon fa-flickr"><span>Flickr</span></a></li>
								<li><a href="#" class="icon fa-vimeo"><span>Vimeo</span></a></li>
								-->
							</ul>
							<hr />
						</div>
					</div>
					<footer>
						<ul id="copyright">
							<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="#">Microraptor</a></li>
						</ul>
					</footer>
				</div>
			</article>

		<!-- Scripts -->
			<script src="../../assets/js/jquery.min.js"></script>
			<script src="../../assets/js/jquery.scrolly.min.js"></script>
			<script src="../../assets/js/browser.min.js"></script>
			<script src="../../assets/js/breakpoints.min.js"></script>
			<script src="../../assets/js/util.js"></script>
			<script src="../../assets/js/main.js"></script>

	</body>
</html>