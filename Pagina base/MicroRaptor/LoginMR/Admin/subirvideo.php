<?php
session_start();
require '../conexion.php';

if(!isset($_SESSION["id_usuario"])){
   header("Location: index.php");
}
?>

<?php
  $usuario = $mysqli->real_escape_string($_SESSION['id_usuario']);
  $peticion = "SELECT * FROM usuarios WHERE id = '$usuario'";
  $conecta = $mysqli->query($peticion);
  $row = $conecta->fetch_array(MYSQLI_ASSOC);
?>

<?php   


require 'perfil/valida_video.php';

$errors = array();

if(isset($_POST['submit'])){

$usuario = $mysqli->real_escape_string($_POST['id']);

$descripcion = $mysqli->real_escape_string($_POST['descripcion']);

$id_insert = $id;

if(isNull($descripcion)) {
        $errors[] = "No puede dejar el campo Descripcion vacio";
        }
        if(count($errors) == 0)
    {
if($_FILES["archivo"] ["error"]>0) {
                echo "<h3><font color='red'><center>No hubo cambios</h3></center>";
                echo "<h1><font color='green'><center>ESTE ES EL ERROR</h1></center>";
                echo "<pre>";
                print_r($_FILES["archivo"] ["error"]);
                echo "</pre>";
                header("Refresh: 0.5; galeria_video.php?id_usuario=".$_POST['id']);
            } else {
               $permitidos = array("video/mp4", "video/mpg", "video/mpeg", "video/avi");
               $limite_kb = 200000;

            if (in_array($_FILES["archivo"]["type"], $permitidos) && $_FILES["archivo"]["size"] <= $limite_kb * 1024) {
            $ruta = 'galeria_video/'.$id_insert.'/';
            $archivo = utf8_decode($ruta.$_FILES["archivo"]["name"]);

            if(!file_exists($ruta)) {
                mkdir($ruta);
            }

            $nombre_archivo = "archivo_subido.mp4";
            $nombre_archivo = uniqid().".mp4";
            $archivo = $ruta . $nombre_archivo; 

            $sql = "INSERT INTO videos (usuario, fecha, archivo, descripcion) VALUES (?, NOW(), ?, ?)";
            $statement = $mysqli->prepare($sql);
                if ($statement) {
            $statement->bind_param("iss", $usuario, $archivo, $descripcion);
            $statement->execute();
            $statement->close();

            }

            if(!file_exists($archivo)) {

            $resultado = @move_uploaded_file($_FILES["archivo"]["tmp_name"], $archivo);

            if($resultado){
                echo "<h3><font color='#2ecc71'><center>Video Actualizado</h3></center>";
                header("Refresh: 0.5; galeria_video.php?id_usuario=".$_POST['id']);
            } else {
            echo "<h3><font color='red'><center>Error al guardar archivo</h3></center>";
                header("Refresh: 0.5; galeria_video.php?id_usuario=".$_POST['id']);
            }               

            } else {
            echo "<h3><font color='red'><center>Archivo ya existe</h3></center>";
            header("Refresh: 0.5; galeria_video.php?id_usuario=".$_POST['id']);
            }               

            } else {
              echo "<h3><font color='red'><center>Archivo no permitido o excede el tama&ntilde;o</h3></center>";
              header("Refresh: 0.5; galeria_video.php?id_usuario=".$_POST['id']);

            }
            }
    }
}            
?>

<body>

    <div class="container">
        <div class="row">
            <h3 style="text-align:center"><?php echo $row['nombre']?></h3>
        </div>

            <form class="form-horizontal" method="POST" action = "<?php $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" autocomplete="off">

                <input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>" />

                <div class="form-group">
                <label for="archivo" class="col-sm-2 control-label">Archivo</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" name="archivo">
                    <br>    
                </div>
            </div>
                <label for="archivo" class="col-sm-2 control-label">Agrega la descripcion de tu video</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="descripcion" required>
                    <br>
                </div>
                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                        <br>
                    <a href="perfil1.php?id_usuario=<?php echo $_SESSION['id_usuario']; ?>" class="btn btn-default">Cancelar</a>

                    <button type = "submit" name = "submit" class="btn btn-primary">Agregar</button>
                </div>
            </div>

        </form> 
        <?php echo resultBlock($errors); ?>
    </div>