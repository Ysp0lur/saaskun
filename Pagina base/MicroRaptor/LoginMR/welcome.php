<?php
	session_start(); //Inicia una nueva sesion o reanuda la existente
	require 'conexion.php'; //Agregamos el script de Conexion
	
//Evaluamos si existe la variable de sesion id_usuario, si no existe redirigimos al index
	if(!isset($_SESSION["id_usuario"])){
		header("Location: login.php");
	}
	
	$idUsuario = $_SESSION['id_usuario'];

	//Consultamos los datos del Usuario
	$sql = "SELECT u.id, u.usuario FROM usuarios AS u INNER JOIN personal AS p ON u.id_personal=p.id WHERE u.id = '$idUsuario'";
	$result=$mysqli->query($sql);
  $row = $result->fetch_assoc();
  //Consulta la tabla: 
$login = $mysqli->query("SELECT * FROM usuario"); 
?>

<html>

<head>
    <title>Inicio &mdash; Abstergo Industries</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="../fonts/icomoon/style.css">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/menu.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">

    <link rel="stylesheet" href="../css/lightgallery.min.css">    
    
    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="../fonts/flaticon/font/flaticon.css">
    
    <link rel="stylesheet" href="../css/swiper.css">

    <link rel="stylesheet" href="../css/aos.css">

    <link rel="stylesheet" href="../css/style.css">
    
  </head>

	
	
<!-- Evaluamos el perfil, si es administrador muestra la opci�n para registrar mas usuarios  -->
	

		<body>
  
 
      
    </header>
    <?php if($_SESSION['tipo_usuario']==1) { ?>
	

  <div class="site-wrap">

<div class="site-mobile-menu">
  <div class="site-mobile-menu-header">
    <div class="site-mobile-menu-close mt-3">
      <span class="icon-close2 js-menu-toggle"></span>
    </div>
  </div>
  <div class="site-mobile-menu-body"></div>
</div>

<header class="site-navbar py-3 border-bottom" role="banner">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" href="css/custom.css">	
  <div class="container-fluid">
    <div class="row align-items-center">
      
      <div class="col-6 col-xl-2" data-aos="fade-down">
        
      </div>
      <div class="col-10 col-md-8 d-none d-xl-block" data-aos="fade-down">
        <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">
        <nav class="menu">
        
<ul class="active">
<li class="current-item"><a href="welcome.php">Inicio</a></li>
<li><a href="registro.php">Registar Usuarios</a></li>
<li><a href="Admin/Dashboard.php">Dashboard</a></li>
<li><a href="usuarios.php">Usuarios</a></li>
<li><a href="../about.html">Ayuda</a></li>
<li><a href="../contact.html">Contactanos</a></li>
<li><a href="logout.php">Cerrar Sesi&oacute;n</a></li>
</ul>

<a class="toggle-nav" href="#">&#9776;</a>

<form class="search-form">
<input type="text">
<button>Buscar</button>
</form>
</nav>
          
        </nav>
      </div>

      <div class="col-6 col-xl-2 text-right" data-aos="fade-down">
        <div class="d-none d-xl-inline-block">
          <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
            <li>
              <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
            </li>
            <li>
              <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
            </li>
            <li>
              <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
            </li>
            <li>
              <a href="#" class="pl-3 pr-3"><span class="icon-youtube-play"></span></a>
            </li>
          </ul>
        </div>

        <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

      </div>

    </div>
  </div>
  <h1><?php echo 'Bienvenido Administrador'.utf8_decode($row['u.usuario']); ?></h1>

	<?php } ?>
	

	<?php if($_SESSION['tipo_usuario']==2) { ?>
	<!-- Imprimimos el tipo de usuario con datos de la consulta  -->
	
	<div class="site-wrap">

<div class="site-mobile-menu">
  <div class="site-mobile-menu-header">
    <div class="site-mobile-menu-close mt-3">
      <span class="icon-close2 js-menu-toggle"></span>
    </div>
  </div>
  <div class="site-mobile-menu-body"></div>
</div>

<header class="site-navbar py-3 border-bottom" role="banner">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" href="css/custom.css">	
  <div class="container-fluid">
    <div class="row align-items-center">
      
      <div class="col-6 col-xl-2" data-aos="fade-down">
        
      </div>
      <div class="col-10 col-md-8 d-none d-xl-block" data-aos="fade-down">
        <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">
        <nav class="menu">
<ul class="active">
<li class="current-item"><a href="welcome.php">Inicio</a></li>

<li><a href="Alumno/Cursos.php">Mis Cursos</a></li>

<li><a href="../about.html">Ayuda</a></li>
<li><a href="../contact.html">Contactanos</a></li>
<li><a href="logout.php">Cerrar Sesi&oacute;n</a></li>
</ul>

<a class="toggle-nav" href="#">&#9776;</a>

<form class="search-form">
<input type="text">
<button>Buscar</button>
</form>
</nav>
          
        </nav>
      </div>

      <div class="col-6 col-xl-2 text-right" data-aos="fade-down">
        <div class="d-none d-xl-inline-block">
          <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
            <li>
              <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
            </li>
            <li>
              <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
            </li>
            <li>
              <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
            </li>
            <li>
              <a href="#" class="pl-3 pr-3"><span class="icon-youtube-play"></span></a>
            </li>
          </ul>
        </div>

        <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

      </div>

    </div>
  </div>
  <h1><?php echo 'Bienvenido Alumno'.utf8_decode($row['u.usuario']); ?></h1>
	<br />
	<?php } ?>

	
 

  <div class="footer py-4">
    <div class="container-fluid text-center">
      <p>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> Todos los Derechos Reservados <i class="icon-heart-o" aria-hidden="true"></i> by <a href="" target="_blank" >MicroRaptor</a>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
    </div>
  </div>

    

    
    
  </div>

  <script src="../js/jquery-3.3.1.min.js"></script>
  <script src="../js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../js/jquery-ui.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/menu.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/jquery.stellar.min.js"></script>
  <script src="../js/jquery.countdown.min.js"></script>
  <script src="../js/jquery.magnific-popup.min.js"></script>
  <script src="../js/bootstrap-datepicker.min.js"></script>
  <script src="../js/swiper.min.js"></script>
  <script src="../js/aos.js"></script>

  <script src="../js/picturefill.min.js"></script>
  <script src="../js/lightgallery-all.min.js"></script>
  <script src="../js/jquery.mousewheel.min.js"></script>

  <script src="../js/main.js"></script>
  
  <script>
    $(document).ready(function(){
      $('#lightgallery').lightGallery();
    });
  </script>
    <!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
	<!-- Imprimimos el tipo de usuario con datos de la consulta  -->
	
	<br />
	</body>
</html>