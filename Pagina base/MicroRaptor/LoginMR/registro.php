<?php
	
	
	require 'conexion.php';
	
	session_start();

	if(!isset($_SESSION["id_usuario"]) || $_SESSION['tipo_usuario']==2){
		header("Location: login.php");
	}
	
	$sql = "SELECT id, tipo FROM tipo_usuario";
	$result=$mysqli->query($sql);
	$cursos = $mysqli->query("SELECT * FROM cursos as c");
	

	$bandera = false;
	
	if(!empty($_POST))
	{
		$nombre = mysqli_real_escape_string($mysqli,$_POST['nombre']);
		$usuario = mysqli_real_escape_string($mysqli,$_POST['usuario']);
		$password = mysqli_real_escape_string($mysqli,$_POST['password']);
		$tipo_usuario = $_POST['tipo_usuario'];
		$sha1_pass = sha1($password);
		

		$error = '';
		
		$sqlUser = "SELECT id FROM usuarios WHERE usuario = '$usuario'";
		$resultUser=$mysqli->query($sqlUser);
		$rows = $resultUser->num_rows;
		
		if($rows > 0) {
			$error = "El usuario ya existe";
			} else {
			
			$sqlPerson = "INSERT INTO personal (nombre) VALUES('$nombre')";
			$resultPerson=$mysqli->query($sqlPerson);
			$idPersona = $mysqli->insert_id;
			
			$sqlUsuario = "INSERT INTO usuarios (usuario, password, id_personal, id_tipo, tipo) VALUES('$usuario','$sha1_pass','$idPersona','$tipo_usuario','Usuario')";
			$resultUsuario = $mysqli->query($sqlUsuario);
			$insertcurso="INSERT INTO alumnocursos(id, id_curso) values(20, 1)";
			
			if($resultUsuario>0)
			$bandera = true;
			else
			$error = "Error al Registrar";
		}
	}
?>

<html>
	<head>
		<title>Registro</title>
		
		<script>
			function validarNombre()
			{
				valor = document.getElementById("nombre").value;
				if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
					alert('Falta Llenar Nombre');
					return false;
				} else { return true;}
			}
			
			function validarUsuario()
			{
				valor = document.getElementById("usuario").value;
				if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
					alert('Falta Llenar Usuario');
					return false;
				} else { return true;}
			}
			
			function validarPassword()
			{
				valor = document.getElementById("password").value;
				if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
					alert('Falta Llenar Contraseña');
					return false;
					} else { 
					valor2 = document.getElementById("con_password").value;
					
					if(valor == valor2) { return true; }
					else { alert('Las contraseñas no coinciden'); return false;}
				}
			}
			
			function validarTipoUsuario()
			{
				indice = document.getElementById("tipo_usuario").selectedIndex;
				if( indice == null || indice==0 ) {
					alert('Seleccione tipo de usuario');
					return false;
				} else { return true;}
			}
			
			function validar()
			{
				if(validarNombre() && validarUsuario() && validarPassword() && validarTipoUsuario())
				{
					$mysqli->query($insertcurso);
					document.registro.submit();
					
					
				}
			}
		</script>
		
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="../fonts/icomoon/style.css">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">

    <link rel="stylesheet" href="../css/lightgallery.min.css">    
    
    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="../fonts/flaticon/font/flaticon.css">
    
    <link rel="stylesheet" href="../css/swiper.css">

    <link rel="stylesheet" href="../css/aos.css">

    <link rel="stylesheet" href="../css/style.css">
	</head>
	
	<body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar py-3 border-bottom" role="banner">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="css/custom.css">	
      <div class="container-fluid">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2" data-aos="fade-down">
            <h1 class="mb-0"><a href="../index.html" class="text-black h2 mb-0">Inicio</a></h1>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block" data-aos="fade-down">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li><a href="welcome.php">Perfil</a></li>
                <li class="active"><a href="../about.html">Ayuda</a></li>
                <li><a href="../contact.html">Contactanos</a></li>
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right" data-aos="fade-down">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3"><span class="icon-youtube-play"></span></a>
                </li>
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>
   
		

        <div class=""  data-aos="fade">
    <div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">		
				<div class="card">
					<div class="loginBox">
						<img src="images/microraptorlogoppal.png" class="img-responsive" alt="logoppal">
						<h2>Registrar</h2>

            
                        <form id="registro" name="registro" action="<?php $_SERVER['PHP_SELF']; ?>" method="POST" >                          	
							<div class="form-group">									
							<input type="text" class="form-control input-lg" id="nombre" name="nombre" placeholder="Nombre" required>        
							</div>
                            <div class="form-group">									
							<input type="text" class="form-control input-lg" id="usuario" name="usuario" placeholder="Usuario" required>        
							</div>							
							<div class="form-group">        
							<input type="password" class="form-control input-lg" id="password" name="password" placeholder="Contraseña" required>       
							</div>
                            <div class="form-group">        
							<input type="password" class="form-control input-lg" id="con_password" name="con_password" placeholder="Confirmar Contraseña" required>       
							</div>
                            <div><label>Tipo Usuario:</label>
				            <select id="tipo_usuario" name="tipo_usuario">
				        	<option value="0">Seleccione tipo de usuario...</option>
				        	<?php while($row = $result->fetch_assoc()){ ?>
					    	<option value="<?php echo $row['id']; ?>"><?php echo $row['tipo']; ?></option>
					        <?php }?>
				            </select>
			                </div>
											<div><label>Curso:</label>
											<select id="id_curso" name="nombre">
				        	<option value="0">Seleccione Curso</option>
				        	<?php while($lc = $cursos->fetch_assoc()){ ?>
                    
					    	<option value="<?php echo $lc['id_curso']; ?>"><?php echo $lc['nombre']; ?></option>
              
                  <?php }?>
				            </select></div>
                            <br/>

							<button class="btn btn-success btn-block" onClick="validar();">Registrar</button>        
							<br/>
						</form>	
                        <?php if($bandera) { ?>
		                	<h1>Registro exitoso</h1>
		                	<a href="welcome.php">Regresar</a>
			
		                	<?php }else{ ?>
			                <br />
		                    <div style = "font-size:16px; color:#cc0000;"><?php echo isset($error) ? utf8_decode($error) : '' ; ?></div>
			
	    	            <?php } ?>

						<hr><p>Problemas? <a href="../contact.html" title="Contactanos">Contactanos al Servicio Tecnico</a>.</p>								
					</div><!-- /.loginBox -->	
				</div><!-- /.card -->
			</div><!-- /.col -->
		</div><!--/.row-->
	</div>
  </div>









		
		

	
 

  <div class="footer py-4">
    <div class="container-fluid text-center">
      <p>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> Todos los Derechos Reservados <i class="icon-heart-o" aria-hidden="true"></i> by <a href="" target="_blank" >MicroRaptor</a>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
    </div>
  </div>

    

    
    
  </div>

  <script src="../js/jquery-3.3.1.min.js"></script>
  <script src="../js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../js/jquery-ui.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/jquery.stellar.min.js"></script>
  <script src="../js/jquery.countdown.min.js"></script>
  <script src="../js/jquery.magnific-popup.min.js"></script>
  <script src="../js/bootstrap-datepicker.min.js"></script>
  <script src="../js/swiper.min.js"></script>
  <script src="../js/aos.js"></script>

  <script src="../js/picturefill.min.js"></script>
  <script src="../js/lightgallery-all.min.js"></script>
  <script src="../js/jquery.mousewheel.min.js"></script>

  <script src="../js/main.js"></script>
  
  <script>
    $(document).ready(function(){
      $('#lightgallery').lightGallery();
    });
  </script>
    <!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
	<!-- Imprimimos el tipo de usuario con datos de la consulta  -->
	
	<br />
	</body>
</html>
