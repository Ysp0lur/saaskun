import React, {Component} from 'react';
import CanvasJSReact from "../../Data/Canvas/canvasjs.react"
// var CanvasJSReact = require('../../Data/Canvas/canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var updateInterval = 500;

class Graficatiemporeal extends Component{
	constructor() {
		super();
		this.updateChart = this.updateChart.bind(this);
	}
	componentDidMount(){
		setInterval(this.updateChart, updateInterval);
	}
	updateChart() {
		var dpsColor, dpsTotal = 0, deltaY, yVal;
		var dps = this.chart.options.data[0].dataPoints;
		var chart = this.chart;
		for (var i = 0; i < dps.length; i++) {
			deltaY = Math.round(2 + Math.random() *(-2-2));
			yVal = deltaY + dps[i].y > 0 ? (deltaY + dps[i].y < 100 ? dps[i].y + deltaY : 100) : 0;
			dpsColor = yVal >= 90 ? "#e40000" : yVal >= 70 ? "#ec7426" : yVal >= 50 ? "#81c2ea" : "#88df86 ";
			dps[i] = {label: "Core "+(i+1) , y: yVal, color: dpsColor};
			dpsTotal += yVal;
		}
		chart.options.data[0].dataPoints = dps;
		chart.options.title.text = "Uso de CPU " + Math.round(dpsTotal / 6) + "%";
		chart.render();
	}
	render() {
		const options = {
			theme: "dark2",
			title: {
				text: "CPU Uso"
			},
			subtitles: [{
				text: "Intel Core i7 6700HQ @ 3.33GHz"
			}],
			axisY: {
				title: "CPU Uso (%)",
				suffix: "%",
			maximum: 100
			},
			data: [{
				type: "column",
				yValueFormatString: "#,###'%'",
				indexLabel: "{y}",
				dataPoints: [
					{ label: "Core 1", y: 68 },
					{ label: "Core 2", y: 3 },
					{ label: "Core 3", y: 8 },
					{ label: "Core 4", y: 87 },
					{ label: "Core 5", y: 2 },
					{ label: "Core 6", y: 6 }
				]
			}]
		}
		return (
			<div>
				<CanvasJSChart options = {options}
					 onRef={ref => this.chart = ref}
				/>
				{/* Puede obtener una referencia a la instancia del gráfico como se muestra arriba usando onRef. Esto le permite acceder a todas las propiedades y métodos del gráfico */}
			</div>
		);
	}
}
export default Graficatiemporeal;