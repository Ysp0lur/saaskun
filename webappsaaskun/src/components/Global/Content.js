import React, {Component} from 'react';
import './css/Content.css';
import MultipleSelect from './MultipleSelect';
import Graficatiemporeal from './Graficatiemporeal';
import CustomizedSlider from './Slider';
import SpeedDials from './Speeddial';

class Content extends Component{
  constructor(){
    super();
    this.state= {
      count: 0
    };
    this.handleCountClick=this.handleCountClick.bind(this);
  }
  
  componentDidMount(){
    this.setState ({
    count:1
    });
  }
  handleCountClick(e){
    if (e.target.id==='add'){

      this.setState({

        count: this.state.count+1
      });
    }else if (e.target.id==='subtract' && this.state.count>0){
      this.setState({
        count: this.state.count-1
      });
    }else{
      this.setState({
        count:0
      });
    }

  }
render() {
  return (
    <div className="Content">
    <h1>Counter: {this.state.count}</h1>
    <button id="add" onClick={this.handleCountClick}>+</button>
    <button id="subtract" onClick={this.handleCountClick}>-</button>
    <button id="reset" onClick={this.handleCountClick}>Reset</button>
    
    <div><SpeedDials/></div>
    <div><CustomizedSlider/></div> 
    <div><Graficatiemporeal/></div>
    {/* <div><MultipleSelect/></div>*/}
    <p>Aqui termina el content</p>
    </div>
  );
}
}
export default Content;
