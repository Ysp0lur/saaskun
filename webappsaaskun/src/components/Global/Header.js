// Dependencies
import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Assets
import './css/Header.css';
import './css/Menu.css';
import PrimarySearchAppBar from '../Global/Appbar';

class Header extends Component{
  static propTypes = {

    title: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired
  };
  render(){
 //console.log(this.props);
  const{title,items} = this.props;
  
  return (
    <div className="Header">

         <div><PrimarySearchAppBar/></div>  
    </div>
  );
}
}
export default Header;
