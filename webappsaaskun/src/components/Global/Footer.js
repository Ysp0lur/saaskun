// Dependencies  
import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Assets
import './css/Footer.css';

class Footer extends Component{
  static propTypes = {
    copyright: PropTypes.string
  };
  render(){
    const { copyright= '&copy; SaasKun 2019'}=this.props;
  return (
    <div className="Footer">
      <p>{copyright}</p>
      <a
          className="App-link"
          href="http://www.redcorp.tk"
          target="_blank"
          rel="noopener noreferrer"
        >
          Visita nuestra Pagina anterior
        </a>
    </div>
  );
}
}
export default Footer;
